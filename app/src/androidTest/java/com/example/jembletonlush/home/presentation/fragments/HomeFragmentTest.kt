package com.example.jembletonlush.home.presentation.fragments

import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.jembletonlush.MainActivity
import junit.framework.TestCase
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.example.jembletonlush.R
import com.example.jembletonlush.utils.EspressoIdlingResourceProvider
import org.junit.After
import org.junit.Before

@RunWith(AndroidJUnit4ClassRunner::class)
class HomeFragmentTest : TestCase(){

    @get: Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun registerIdlingResource(){
        IdlingRegistry.getInstance().register(EspressoIdlingResourceProvider.idlingResourceCount)
    }

    @After
    fun unregisterIdlingResource(){
        IdlingRegistry.getInstance().unregister(EspressoIdlingResourceProvider.idlingResourceCount)
    }

    @Test
    fun recyclerViewIsDisplayedTest(){
        Espresso.onView(withId(R.id.rv_launchesRecyclerView)).check(matches(isDisplayed()))
    }



}


