package com.example.jembletonlush.utils

import android.content.Context
import android.net.Uri
import android.os.Build
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.facebook.imagepipeline.nativecode.ImagePipelineNativeLoader
import java.io.File
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


fun Context.initFresco() {

    val builder = ImagePipelineConfig.newBuilder(this.applicationContext)
    var imagePipelineConfig = builder.setDownsampleEnabled(true)
        .setDiskCacheEnabled(true)
        .build()
    Fresco.initialize(this.applicationContext, imagePipelineConfig)

    try {
        ImagePipelineNativeLoader.load()
    } catch (error: UnsatisfiedLinkError) {
        Fresco.shutDown()
        builder.experiment().setNativeCodeDisabled(true)
        imagePipelineConfig = builder.build()
        Fresco.initialize(this.applicationContext, imagePipelineConfig)
        error.printStackTrace()
    }
}


@BindingAdapter("setImageSrc")
fun SimpleDraweeView.setImageSrc(src: String?) {
    this.setImageURI(src)

}

@BindingAdapter("fromFilePath")
fun SimpleDraweeView.fromFilePath(path: String?) {
    if (path.isNullOrEmpty())
        return

    val uri = Uri.fromFile(File(path))
    this.setImageURI(uri.clearFrescoCash())
}

fun Uri.clearFrescoCash(): Uri {
    val imagePipeline = Fresco.getImagePipeline()
    imagePipeline.evictFromCache(this)
    imagePipeline.clearCaches()

    return this
}

const val INPUT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
const val OUTPUT_FORMAT = "dd-MM-yyyy"

@BindingAdapter("launch_date")
fun convertDateTime(tv: TextView, date: String) {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val outFormatter = DateTimeFormatter.ofPattern(OUTPUT_FORMAT, Locale.US)
        val inFormatter = DateTimeFormatter.ofPattern(INPUT_FORMAT, Locale.US)
        val localDate = LocalDateTime.parse(date, inFormatter)
        val formattedDate = outFormatter.format(localDate)
        tv.text = formattedDate
    } else {
        val parser = SimpleDateFormat(INPUT_FORMAT, Locale.US)
        val formatter = SimpleDateFormat(OUTPUT_FORMAT, Locale.US)
        tv.text = formatter.format(parser.parse(date))
    }
}

fun dateTimeConverter(date: String) : String{

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val outFormatter = DateTimeFormatter.ofPattern(OUTPUT_FORMAT, Locale.US)
        val inFormatter = DateTimeFormatter.ofPattern(INPUT_FORMAT, Locale.US)
        val localDate = LocalDateTime.parse(date, inFormatter)
        return outFormatter.format(localDate)
    } else {
        val parser = SimpleDateFormat(INPUT_FORMAT, Locale.US)
        val formatter = SimpleDateFormat(OUTPUT_FORMAT, Locale.US)
        return formatter.format(parser.parse(date))
    }
}