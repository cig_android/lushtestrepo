package com.example.jembletonlush.utils

import androidx.test.espresso.idling.CountingIdlingResource


object EspressoIdlingResourceProvider {

    private const val COUNTER = "GLOBAL"

    @JvmField val idlingResourceCount = CountingIdlingResource(COUNTER)

    fun increment(){
        idlingResourceCount.increment()
    }

    fun decrement(){
        if(!idlingResourceCount.isIdleNow){
            idlingResourceCount.decrement()
        }
    }
}