//package com.example.jembletonlush.home.presentation.show_all_launches
//
//import android.view.LayoutInflater
//import android.view.ViewGroup
//import androidx.recyclerview.widget.DiffUtil
//import androidx.recyclerview.widget.ListAdapter
//import androidx.recyclerview.widget.RecyclerView
//import com.example.jembletonlush.data.domain.LaunchDetailsDomain
//import com.example.jembletonlush.databinding.ItemLaunchDetailBinding



// This was my original adapter. It worked brilliantly on my HTC-11!! But when I ran it on a phone running API level 21 it crashed at this code:
//  val binding = ItemLaunchDetailBinding.inflate(inflater, parent, false). I thought I'd leave this code here to show you my original work.

//class LaunchesAdapter :
//    ListAdapter<LaunchDetailsDomain, LaunchesAdapter.ViewHolder>(LaunchesDiffCallback()) {
//
//    class LaunchesDiffCallback : DiffUtil.ItemCallback<LaunchDetailsDomain>() {
//
//        override fun areItemsTheSame(oldItem: LaunchDetailsDomain, newItem: LaunchDetailsDomain): Boolean {
//            return oldItem.name == newItem.name
//        }
//
//        override fun areContentsTheSame(oldItem: LaunchDetailsDomain, newItem: LaunchDetailsDomain): Boolean {
//            return oldItem == newItem
//        }
//
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        return ViewHolder.form(
//            parent
//        )
//
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.bind(getItem(position))
//
//
//    }
//
//
//    class ViewHolder constructor(private val binding: ItemLaunchDetailBinding) : RecyclerView.ViewHolder(binding.root){
//
//        fun bind(item: LaunchDetailsDomain){
//            binding.model = item
//
//            binding.executePendingBindings()
//        }
//
//        companion object {
//            fun form(parent: ViewGroup): ViewHolder {
//                val inflater = LayoutInflater.from(parent.context)
//                val binding = ItemLaunchDetailBinding.inflate(inflater, parent, false)
//                return ViewHolder(
//                    binding
//                )
//            }
//        }
//    }
//}