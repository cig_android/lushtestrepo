package com.example.jembletonlush.home.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jembletonlush.data.domain.LaunchDetailsDomain
import com.example.jembletonlush.remote.LaunchRemote
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class HomeViewModel(
    private val launchRemote: LaunchRemote
) : ViewModel() {

    var launches = MutableLiveData<List<LaunchDetailsDomain>>()

    val errorMessage = MutableLiveData<String>()

    fun getAllLaunchDetails() = viewModelScope.launch{
        withContext(Dispatchers.IO){
            try{
                val launchInfo = launchRemote.getAllLaunchDetails()
               launches.postValue(launchInfo)
            } catch (throwable: Throwable){
                when (throwable){
                    is IOException -> {
                        errorMessage.postValue("Network Error")
                    }
                    is HttpException -> {
                        val codeError = throwable.code()
                        val errorMessageResponse = throwable.message()
                        errorMessage.postValue("Error $errorMessageResponse : $codeError")
                    }
                    else -> {
                        errorMessage.postValue("Unknown error")
                    }
                }
            }
        }
    }
}