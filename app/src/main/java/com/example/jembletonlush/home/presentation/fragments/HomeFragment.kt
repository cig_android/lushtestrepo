package com.example.jembletonlush.home.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.jembletonlush.data.domain.LaunchDetailsDomain
import com.example.jembletonlush.databinding.FragmentHomeBinding
import com.example.jembletonlush.home.presentation.show_all_launches.LaunchDetailsAdapter
import com.example.jembletonlush.home.presentation.viewmodels.HomeViewModel
import com.example.jembletonlush.utils.EspressoIdlingResourceProvider
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private val mViewModel: HomeViewModel by viewModel()
    private lateinit var adapter: LaunchDetailsAdapter

    private lateinit var mBinding: FragmentHomeBinding
    private var launchList : List<LaunchDetailsDomain> = listOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentHomeBinding.inflate(layoutInflater)
        mBinding.lifecycleOwner = viewLifecycleOwner

        registerObservers()

        adapter = LaunchDetailsAdapter(launchList)
        mBinding.rvLaunchesRecyclerView.adapter = adapter
        populateLaunchDetails()

        return mBinding.root
    }

    private fun registerObservers() {

        mViewModel.launches.observe(viewLifecycleOwner, { launchesData ->
            EspressoIdlingResourceProvider.increment()
            adapter.submitList(launchesData)
            adapter.notifyDataSetChanged()
            mBinding.pbProgressBar.isVisible = false
            EspressoIdlingResourceProvider.decrement()
        })
    }

    private fun populateLaunchDetails() {

        mViewModel.getAllLaunchDetails()
    }
}