package com.example.jembletonlush.home.presentation.show_all_launches

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.jembletonlush.R
import com.example.jembletonlush.data.domain.LaunchDetailsDomain
import com.example.jembletonlush.utils.dateTimeConverter
import com.facebook.drawee.view.SimpleDraweeView

class LaunchDetailsAdapter(private var items: List<LaunchDetailsDomain>) :
    RecyclerView.Adapter<LaunchDetailsAdapter.LaunchesViewHolder>() {

    class LaunchesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val patchImage: ImageView = view.findViewById(R.id.simpleDraweeView) as SimpleDraweeView
        val title: TextView = view.findViewById(R.id.tv_launchDetailHeader)
        val launchDate: TextView = view.findViewById(R.id.iv_launchDate)
        val isSuccess: ImageView = view.findViewById(R.id.iv_successFailure)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LaunchesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_launch_no_binding, parent, false)
        return LaunchesViewHolder(view)
    }

    override fun onBindViewHolder(holder: LaunchesViewHolder, position: Int) {
        val item = items[position]
        val imageUri = Uri.parse(item.patchImage)
        holder.patchImage.setImageURI(imageUri)
        holder.title.text = item.name
        holder.launchDate.text = dateTimeConverter(item.date)

        val imageRes = if (item.isSuccess) {
            R.drawable.tick_success
        } else {
            R.drawable.cross_failure
        }
        holder.isSuccess.setImageResource(imageRes)
    }

    fun submitList(launches: List<LaunchDetailsDomain>) {
        items = launches
    }

    override fun getItemCount(): Int {
        return items.size
    }
}