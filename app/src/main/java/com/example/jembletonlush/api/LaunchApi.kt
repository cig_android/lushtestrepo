package com.example.jembletonlush.api

import com.example.jembletonlush.data.models.LaunchDetailsEntity
import retrofit2.http.GET
import retrofit2.http.Headers

interface LaunchApi {

    @Headers("Accept: application/json")
    @GET("launches")
    suspend fun getLaunchDetails2() : List<LaunchDetailsEntity>
}