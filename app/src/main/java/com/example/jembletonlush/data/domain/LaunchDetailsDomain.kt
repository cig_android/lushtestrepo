package com.example.jembletonlush.data.domain

data class LaunchDetailsDomain(
    val date: String,
    val name: String,
    val isSuccess: Boolean,
    val patchImage: String?
)


