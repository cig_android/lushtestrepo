package com.example.jembletonlush.data.models


import com.example.jembletonlush.data.domain.LaunchDetailsDomain
import com.google.gson.annotations.SerializedName

data class LaunchDetailsEntity(
    @SerializedName("date_utc")
    val dateUtc: String,
    @SerializedName("links")
    val links: Links,
    @SerializedName("name")
    val name: String,
    @SerializedName("success")
    val success: Boolean
)

data class Links(
    @SerializedName("patch")
    val patch: Patch
)

data class Patch(
    @SerializedName("large")
    val large: String?,
    @SerializedName("small")
    val small: String?
)

fun LaunchDetailsEntity.mapToDomain() =
    LaunchDetailsDomain(
        name = name,
        date = dateUtc,
        isSuccess = success,
        patchImage = links.patch.small
    )
