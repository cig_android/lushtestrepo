package com.example.jembletonlush

import android.content.Context
import com.example.jembletonlush.api.LaunchApi
import com.example.jembletonlush.home.presentation.viewmodels.HomeViewModel
import com.example.jembletonlush.remote.LaunchRemote
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.scope.Scope
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Dependencies {

    private const val CONNECT_TIMEOUT = 15L
    private const val WRITE_TIMEOUT = 15L
    private const val READ_TIMEOUT = 15L

    private val appModule = module {
        single {
            androidContext().getSharedPreferences("prefs", Context.MODE_PRIVATE)
        }
    }

    private val retrofitModule = module {
        single { Cache(androidApplication().cacheDir, 40L * 1024 * 1024) }
        single { GsonBuilder().create() }
        single { retrofitHttpClient() }
        single { retrofitBuilder() }
        single {
            Interceptor { interceptor ->
                interceptor.proceed(interceptor.request().newBuilder().apply {
                    header("Accept", "application/json")
                }.build())
            }
        }
    }

    private fun Scope.retrofitBuilder(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .client(get())
            .build()
    }

    private fun Scope.retrofitHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            cache(get())
            connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            retryOnConnectionFailure(true)
        }.build()
    }

    private val homeModule = module {
        viewModel { HomeViewModel(get()) }
        single {LaunchRemote(get())}
        single(createdAtStart = false) { get<Retrofit>().create((LaunchApi::class.java))}
    }

    val modules = listOf(
        appModule,
        retrofitModule,
        homeModule
    )
}