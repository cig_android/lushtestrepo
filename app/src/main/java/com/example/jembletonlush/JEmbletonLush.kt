package com.example.jembletonlush

import android.app.Application
import com.example.jembletonlush.utils.initFresco
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class JEmbletonLush : Application() {

    override fun onCreate() {
        super.onCreate()

        initFresco()
        starKoinApp()
    }

    private fun starKoinApp(){
        startKoin{
            androidContext(applicationContext)
            modules(Dependencies.modules)
        }
    }
}