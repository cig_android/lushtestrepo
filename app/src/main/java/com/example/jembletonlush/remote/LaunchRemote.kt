package com.example.jembletonlush.remote

import com.example.jembletonlush.api.LaunchApi
import com.example.jembletonlush.data.domain.LaunchDetailsDomain
import com.example.jembletonlush.data.models.mapToDomain

class LaunchRemote(
    private val api: LaunchApi
) {
    suspend fun getAllLaunchDetails(): List<LaunchDetailsDomain> {
        val data = api.getLaunchDetails2()
        return data.map { it.mapToDomain() }
    }
}

